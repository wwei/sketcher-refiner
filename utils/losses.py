import keras.backend as K
import numpy as np


def wasserstein_loss(y_true, y_pred):
    """Calculates the Wasserstein loss for a sample batch.
    The Wasserstein loss function is very simple to calculate. In a standard GAN, the
    discriminator has a sigmoid output, representing the probability that samples are
    real or generated. In Wasserstein GANs, however, the output is linear with no
    activation function! Instead of being constrained to [0, 1], the discriminator wants
    to make the distance between its output for real and generated samples as
    large as possible.
    The most natural way to achieve this is to label generated samples -1 and real
    samples 1, instead of the 0 and 1 used in normal GANs, so that multiplying the
    outputs by the labels will give you the loss immediately.
    Note that the nature of this loss means that it can be (and frequently will be)
    less than 0."""
    return K.mean(y_true * y_pred)


def gradient_penalty_loss(y_true, y_pred, averaged_samples,
                          gradient_penalty_weight):
    """Calculates the gradient penalty loss for a batch of "averaged" samples.
    In Improved WGANs, the 1-Lipschitz constraint is enforced by adding a term to the
    loss function that penalizes the network if the gradient norm moves away from 1.
    However, it is impossible to evaluate this function at all points in the input
    space. The compromise used in the paper is to choose random points on the lines
    between real and generated samples, and check the gradients at these points. Note
    that it is the gradient w.r.t. the input averaged samples, not the weights of the
    discriminator, that we're penalizing!
    In order to evaluate the gradients, we must first run samples through the generator
    and evaluate the loss. Then we get the gradients of the discriminator w.r.t. the
    input averaged samples. The l2 norm and penalty can then be calculated for this
    gradient.
    Note that this loss function requires the original averaged samples as input, but
    Keras only supports passing y_true and y_pred to loss functions. To get around this,
    we make a partial() of the function with the averaged_samples argument, and use that
    for model training."""
    # first get the gradients:
    #   assuming: - that y_pred has dimensions (batch_size, 1)
    #             - averaged_samples has dimensions (batch_size, nbr_features)
    # gradients afterwards has dimension (batch_size, nbr_features), basically
    # a list of nbr_features-dimensional gradient vectors
    gradients = K.gradients(y_pred, averaged_samples)[0]
    # compute the euclidean norm by squaring ...
    gradients_sqr = K.square(gradients)
    #   ... summing over the rows ...
    gradients_sqr_sum = K.sum(gradients_sqr,
                              axis=np.arange(1, len(gradients_sqr.shape)))
    #   ... and sqrt
    gradient_l2_norm = K.sqrt(gradients_sqr_sum)
    # compute lambda * (1 - ||grad||)^2 still for each single sample
    gradient_penalty = gradient_penalty_weight * K.square(1 - gradient_l2_norm)
    # return the mean as loss over all the batch samples
    return K.mean(gradient_penalty)


def psnr(y_true, y_pred):
    """
    Calculates the PSNR for evaluation.
    """
    mse=np.sqrt(np.mean((y_true-y_pred)**2))
    max_I=np.max([np.max(y_pred),np.max(y_true)])
    return 20.0*np.log10(max_I/mse)

# def l1_loss(y_true, y_pred):
#         """
#     Calculates the L1 loss.
#     """
#     return K.mean(K.abs(y_pred - y_true), axis=-1)

# for the HC, the sum will be 0.5 not 1 (50 not 100)
def calculate_weight(patch_wm, patch_les, patch_other):
    
    
    total = np.sum(patch_other,axis=(1,2,3,4))+np.sum(patch_les,axis=(1,2,3,4))+np.sum(patch_wm,axis=(1,2,3,4))
    
    w_other = (1-np.sum(patch_other,axis=(1,2,3,4))/total)/2
    w_les = (1-np.sum(patch_les,axis=(1,2,3,4))/total)/2
    w_wm = (1-np.sum(patch_wm,axis=(1,2,3,4))/total)/2
    
    
    w = w_wm[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis]*patch_wm+ \
    w_les[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis]*patch_les+ \
    w_other[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis]*patch_other
    
    
    return 100*w

def discriminator_loss(disc_real_output, disc_generated_output):
    real_loss = tf.losses.sigmoid_cross_entropy(multi_class_labels=tf.ones_like(disc_real_output),
                                                logits=disc_real_output)
    generated_loss = tf.losses.sigmoid_cross_entropy(multi_class_labels=tf.zeros_like(disc_generated_output),
                                                     logits=disc_generated_output)

    total_disc_loss = real_loss + generated_loss

    return total_disc_loss


def generator_loss(disc_generated_output, gen_output, target, weight, mask):
    gan_loss = tf.losses.sigmoid_cross_entropy(multi_class_labels = tf.ones_like(disc_generated_output),
                                             logits = disc_generated_output) 
    # mean absolute error
    # l1_loss = tf.reduce_mean(tf.abs(target - gen_output))
    l1_loss = tf.losses.absolute_difference(labels=target,predictions=gen_output,weights=mask)

    total_gen_loss = gan_loss + (weight * l1_loss)

    return total_gen_loss

# def generator_loss(disc_generated_output, gen_output, target, weight):

#     gan_loss = tf.losses.sigmoid_cross_entropy(multi_class_labels=tf.ones_like(disc_generated_output),
#                                                logits=disc_generated_output)
#     # mean absolute error
#     # l1_loss = tf.reduce_mean(tf.abs(target - gen_output))
#     # l1_loss = tf.losses.absolute_difference(labels=target, predictions=gen_output, weights=mask)
#     l1_loss = tf.losses.absolute_difference(labels=target, predictions=gen_output)

#     total_gen_loss = gan_loss + (weight * l1_loss)

#     return total_gen_loss