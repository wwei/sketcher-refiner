import keras.backend as K

from keras.layers.core import Dropout 
from keras.models import Model
from keras.layers import Input,Flatten,Dense,Activation, Lambda, Reshape
from keras.layers.convolutional import Conv3D,UpSampling3D,ZeroPadding3D, Conv3DTranspose
from keras.layers.convolutional import Conv2D,UpSampling2D,ZeroPadding2D, Conv2DTranspose

from keras.layers.pooling import MaxPooling2D,MaxPooling3D
from keras.layers.merge import Concatenate
from keras.layers.normalization import BatchNormalization

from keras.layers.advanced_activations import LeakyReLU
import numpy as np

def conv_block_unet_3D(x, f, name, bn_axis, bn=True, strides=(2,2,2)):

    x = LeakyReLU(0.2)(x)
    x = Conv3D(f, (3, 3,3), strides=strides, name=name, padding="same")(x)
    if bn:
        x = BatchNormalization(axis=bn_axis)(x)

    return x

def up_conv_block_unet_3D(x, x2, f, name, bn_axis, bn=True, dropout=False):

    x = Activation("relu")(x)
    x = UpSampling3DD(size=(2, 2,2))(x)
    x = Conv3D(f, (3, 3), name=name, padding="same")(x)
    if bn:
        x = BatchNormalization(axis=bn_axis)(x)
    if dropout:
        x = Dropout(0.5)(x)
    x = Concatenate(axis=bn_axis)([x, x2])

    return x

def deconv_block_unet_3D(x, x2, f , name, bn_axis, bn=True, dropout=False):

    x = Activation("relu")(x)
    x = Conv3DTranspose(f, (3, 3,3), strides=(2, 2,2), padding="same")(x)
    if bn:
        x = BatchNormalization(axis=bn_axis)(x)
    if dropout:
        x = Dropout(0.5)(x)
    x = Concatenate(axis=bn_axis)([x, x2])

    return x

def UNet_3D_Deconv(img_dim,nb_channels, nb_filters = 64,bn=True,model_name="unet_3D_deconv"):
    """
    3D U-Net architecture. The decoder is built by Deconvolutional neural networks.

    Parameters
    ----------
    img_dim: tuple of ints (img_height, img_weight, img_depth, input_channels).
    
    nb_channels: ints, the number of output channels. 
    
    nb_filters: ints, the number of feature maps for the first layer. Note that the number of feature maps doubles at each pooling.
    
    bn: Boolean, whether the network uses batch normalisation.
    
    model_name: String. The modale name.
        
    Returns(Output shape)
    -------
    (img_height, img_weight, img_depth, nb_channels)

    """ 

    assert K.backend() == "tensorflow", "Not implemented with theano backend"
    
    h, w,d, _ = img_dim
    
    if h & (h-1) !=0:
        raise ValueError("Height %d of the input is not the power of 2." %(h))

    
    if w & (w-1) !=0:
        raise ValueError("Weight %d of the input is not the power of 2." %(w))
    
    if d & (d-1) !=0:
        raise ValueError("Depth %d of the input is not the power of 2." %(d))
    
    bn_axis = -1

    min_s = min(img_dim[:-1])

    unet_input = Input(shape=img_dim, name="unet_input")

    # Prepare encoder filters
    nb_conv = int(np.floor(np.log(min_s) / np.log(2)))
    list_nb_filters = [nb_filters * min(8, (2 ** i)) for i in range(nb_conv)]

    # Encoder
    list_encoder = [Conv3D(list_nb_filters[0], (3, 3,3),
                           strides=(2, 2,2), name="unet_conv3D_1", padding="same")(unet_input)]

    for i, f in enumerate(list_nb_filters[1:]):
        name = "unet_conv3D_%s" % (i + 2)
        conv = conv_block_unet_3D(list_encoder[-1], f, name, bn_axis,bn=bn)
        list_encoder.append(conv)

    # Prepare decoder filters
    list_nb_filters = list_nb_filters[:-1][::-1]
    if len(list_nb_filters) < nb_conv - 1:
        list_nb_filters.append(nb_filters)

    # Decoder
    list_decoder = [deconv_block_unet_3D(list_encoder[-1], list_encoder[-2],
                                      list_nb_filters[0],"unet_upconv2D_1", bn_axis, bn=bn)]
    for i, f in enumerate(list_nb_filters[1:]):
        name = "unet_upconv2D_%s" % (i + 2)
        # Dropout only on first few layers
        if i < 2:
            d = True
        else:
            d = False
        conv = deconv_block_unet_3D(list_decoder[-1], list_encoder[-(i + 3)], f,name, bn_axis, bn=bn, dropout=d)
        list_decoder.append(conv)

    x = Activation("relu")(list_decoder[-1])
    x = Conv3DTranspose(nb_channels, (3, 3,3), strides=(2, 2,2), padding="same")(x)


    x = Activation("relu")(x)

    generator_unet = Model(inputs=[unet_input], outputs=[x],name=model_name)


    return generator_unet

