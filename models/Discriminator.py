import keras.backend as K

from keras.layers.core import Dropout 
from keras.models import Model
from keras.layers import Input,Flatten,Dense,Activation, Lambda, Reshape
from keras.layers.convolutional import Conv3D,UpSampling3D,ZeroPadding3D, Conv3DTranspose
from keras.layers.convolutional import Conv2D,UpSampling2D,ZeroPadding2D, Conv2DTranspose

from keras.layers.pooling import MaxPooling2D,MaxPooling3D
from keras.layers.merge import Concatenate
from keras.layers.normalization import BatchNormalization

from keras.layers.advanced_activations import LeakyReLU
import numpy as np


def Discriminator(img_dim, nb_filters = 64, model_name="discriminator"):
    
    if K.image_dim_ordering() == "channels_first":
        bn_axis = 1
    else:
        bn_axis = -1
        
    min_s = min(img_dim[:-1])
    nb_conv = int(np.floor(np.log(min_s) / np.log(2)))
    
    list_filters = [nb_filters * min(8, (2 ** i)) for i in range(nb_conv)]

    x_input = Input(shape=img_dim, name="discriminator_input")
    x = Conv3D(list_filters[0], (3, 3,3), strides=(2, 2,2), name="disc_conv3d_1", padding="same")(x_input)
    x = BatchNormalization(axis=bn_axis)(x)
    x = LeakyReLU(0.2)(x)    
    
    # Next convs
    for i, f in enumerate(list_filters[1:]):
        name = "disc_conv3d_%s" % (i + 2)
        x = Conv3D(f, (3, 3,3), strides=(2, 2,2), name=name, padding="same")(x)
        x = BatchNormalization(axis=bn_axis)(x)
        x = LeakyReLU(0.2)(x)

    x_flat = Flatten()(x)
    
# First    
#     x_flat = Dense(1024, activation='tanh')(x_flat)
#     x = Dense(1, activation='sigmoid')(x_flat)
    
#Second    
    x = Dense(2, activation="softmax")(x_flat)    

    
    m = Model(inputs=[x_input], outputs=[x], name=model_name)
    
    return m

if __name__ == '__main__':
    Discriminator(img_dim=(32,32,32,1))
