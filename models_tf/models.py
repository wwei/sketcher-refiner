from __future__ import division

import tensorflow as tf

tf.enable_eager_execution()


def hw_flatten(x):
    # Input shape x: [BATCH, HEIGHT, WIDTH, CHANNELS] or [BATCH, DEPTH, HEIGHT, WIDTH, CHANNELS]
    # flat the feature volume across the tensor width , height (and depth).
    x_shape = tf.shape(x)
    return tf.reshape(x, [x_shape[0], -1, x_shape[-1]])  # return [BATCH, W*H, CHANNELS]

class FlexSelfAttention3D(tf.keras.Model):
    def __init__(self, number_of_filters):
        super(FlexSelfAttention3D, self).__init__()

        self.pooling = tf.keras.layers.MaxPool3D(pool_size=(2, 2, 2))

        self.f = tf.keras.layers.Conv3D(number_of_filters // 8, 1,
                                        strides=1, padding='SAME', name="f_x",
                                        activation=None)

        self.g = tf.keras.layers.Conv3D(number_of_filters // 8, 1,
                                        strides=1, padding='SAME', name="g_x",
                                        activation=None)

        self.h = tf.keras.layers.Conv3D(number_of_filters, 1,
                                        strides=1, padding='SAME', name="h_x",
                                        activation=None)

        self.gamma = tf.Variable(0., trainable=True, name="gamma")

        self.flatten = tf.keras.layers.Flatten()

        self.uppool = tf.keras.layers.UpSampling3D(size=(2, 2, 2))

    def call(self, x):

        x = self.pooling(x)

        f = self.f(x)
        g = self.g(x)
        h = self.h(x)

        f_flatten = hw_flatten(f)
        g_flatten = hw_flatten(g)
        h_flatten = hw_flatten(h)

        s = tf.matmul(g_flatten, f_flatten, transpose_b=True)  # [B,N,C] * [B, N, C] = [B, N, N]

        b = tf.nn.softmax(s, axis=-1)
        o = tf.matmul(b, h_flatten)
        y = self.gamma * tf.reshape(o, tf.shape(x)) + x

        return self.uppool(y)

class SelfAttention3D(tf.keras.Model):
    def __init__(self, number_of_filters):
        super(SelfAttention3D, self).__init__()

        self.f = tf.keras.layers.Conv3D(number_of_filters // 8, 1,
                                        strides=1, padding='SAME', name="f_x",
                                        activation=None)

        self.g = tf.keras.layers.Conv3D(number_of_filters // 8, 1,
                                        strides=1, padding='SAME', name="g_x",
                                        activation=None)

        self.h = tf.keras.layers.Conv3D(number_of_filters, 1,
                                        strides=1, padding='SAME', name="h_x",
                                        activation=None)

        self.gamma = tf.Variable(0., trainable=True, name="gamma")

        self.flatten = tf.keras.layers.Flatten()

    def call(self, x):
        f = self.f(x)
        g = self.g(x)
        h = self.h(x)

        f_flatten = hw_flatten(f)
        g_flatten = hw_flatten(g)
        h_flatten = hw_flatten(h)

        s = tf.matmul(g_flatten, f_flatten, transpose_b=True)  # [B,N,C] * [B, N, C] = [B, N, N]

        b = tf.nn.softmax(s, axis=-1)
        o = tf.matmul(b, h_flatten)
        y = self.gamma * tf.reshape(o, tf.shape(x)) + x

        return y


class ResDown(tf.keras.Model):

    def __init__(self, filters, size, apply_batchnorm=True):
        super(ResDown, self).__init__()
        self.apply_batchnorm = apply_batchnorm
        initializer = tf.random_normal_initializer(0., 0.02)

        self.conv1 = tf.keras.layers.Conv3D(filters,
                                            size,
                                            strides=2,
                                            padding='same',
                                            kernel_initializer=initializer,
                                            use_bias=False)


        if self.apply_batchnorm:
            self.batchnorm1 = tf.keras.layers.BatchNormalization()
            self.batchnorm2 = tf.keras.layers.BatchNormalization()
            
        self.conv2 = tf.keras.layers.Conv3D(filters,
                                            size,
                                            strides=2,
                                            padding='same',
                                            kernel_initializer=initializer,
                                            use_bias=False)
        self.conv3 = tf.keras.layers.Conv3D(filters,
                                            size,
                                            strides=1,
                                            padding='same',
                                            kernel_initializer=initializer,
                                            use_bias=False)       
    def call(self, x, training):
        x1 = self.conv1(x)

        if self.apply_batchnorm:
            x = self.batchnorm1(x, training=training)
        x2 = tf.nn.leaky_relu(x)
        x2 = self.conv2(x2)
        if self.apply_batchnorm:
            x2 = self.batchnorm2(x2, training=training)
        x2 = tf.nn.leaky_relu(x2)
        x2 = self.conv3(x2)

        y = tf.add(x1, x2)

        return y


class ResUp(tf.keras.Model):

    def __init__(self, filters, size, apply_dropout=False):
        super(ResUp, self).__init__()
        self.apply_dropout = apply_dropout
        initializer = tf.random_normal_initializer(0., 0.02)

        self.up_conv = tf.keras.layers.Conv3DTranspose(filters,
                                                       size,
                                                       strides=2,
                                                       padding='same',
                                                       kernel_initializer=initializer,
                                                       use_bias=False)

        self.conv1 = tf.keras.layers.Conv3D(filters,
                                            size,
                                            strides=1,
                                            padding='same',
                                            kernel_initializer=initializer,
                                            use_bias=False)   

        self.batchnorm1 = tf.keras.layers.BatchNormalization()
        self.batchnorm2 = tf.keras.layers.BatchNormalization()

        self.conv2 = tf.keras.layers.Conv3D(filters,
                                            size,
                                            strides=1,
                                            padding='same',
                                            kernel_initializer=initializer,
                                            use_bias=False)   

        if self.apply_dropout:
            self.dropout = tf.keras.layers.Dropout(0.5)

    def call(self, i1, i2, training):
        x = self.up_conv(i1)
        x = tf.concat([x, i2], axis=-1)

        x1 = self.conv1(x)

        x2 = self.batchnorm1(x, training=training)
        if self.apply_dropout:
            x2 = self.dropout(x2, training=training)

        x2 = tf.nn.leaky_relu(x2)
        x2 = self.conv2(x2)
        x2 = self.batchnorm2(x2, training=training)
        x2 = tf.nn.leaky_relu(x2)

        y = tf.add(x1, x2)
        return y




class Downsample(tf.keras.Model):

    def __init__(self, filters, size, apply_batchnorm=True):
        super(Downsample, self).__init__()
        self.apply_batchnorm = apply_batchnorm
        initializer = tf.random_normal_initializer(0., 0.02)

        self.conv1 = tf.keras.layers.Conv3D(filters,
                                            size,
                                            strides=2,
                                            padding='same',
                                            kernel_initializer=initializer,
                                            use_bias=False)

        if self.apply_batchnorm:
            self.batchnorm = tf.keras.layers.BatchNormalization()

    def call(self, x, training):
        x = self.conv1(x)
        if self.apply_batchnorm:
            x = self.batchnorm(x, training=training)
        x = tf.nn.leaky_relu(x)
        return x


class Downsample_SA(tf.keras.Model):

    def __init__(self, filters, size, apply_batchnorm=True):
        super(Downsample_SA, self).__init__()
        self.apply_batchnorm = apply_batchnorm
        initializer = tf.random_normal_initializer(0., 0.02)

        self.conv1 = tf.keras.layers.Conv3D(filters,
                                            size,
                                            strides=2,
                                            padding='same',
                                            kernel_initializer=initializer,
                                            use_bias=False)

        if self.apply_batchnorm:
            self.batchnorm = tf.keras.layers.BatchNormalization()

        self.attention = SelfAttention3D(filters)

    def call(self, x, training):
        x = self.conv1(x)
        if self.apply_batchnorm:
            x = self.batchnorm(x, training=training)
        x = tf.nn.leaky_relu(x)
        x = self.attention(x)
        return x


class Upsample(tf.keras.Model):

    def __init__(self, filters, size, apply_dropout=False):
        super(Upsample, self).__init__()
        self.apply_dropout = apply_dropout
        initializer = tf.random_normal_initializer(0., 0.02)

        self.up_conv = tf.keras.layers.Conv3DTranspose(filters,
                                                       size,
                                                       strides=2,
                                                       padding='same',
                                                       kernel_initializer=initializer,
                                                       use_bias=False)

        self.batchnorm = tf.keras.layers.BatchNormalization()
        if self.apply_dropout:
            self.dropout = tf.keras.layers.Dropout(0.5)

    def call(self, x1, x2, training):
        x = self.up_conv(x1)
        x = self.batchnorm(x, training=training)
        if self.apply_dropout:
            x = self.dropout(x, training=training)
        x = tf.nn.relu(x)
        x = tf.concat([x, x2], axis=-1)
        return x


class Upsample_SA(tf.keras.Model):

    def __init__(self, filters, size, apply_dropout=False):
        super(Upsample_SA, self).__init__()
        self.apply_dropout = apply_dropout
        initializer = tf.random_normal_initializer(0., 0.02)

        self.up_conv = tf.keras.layers.Conv3DTranspose(filters,
                                                       size,
                                                       strides=2,
                                                       padding='same',
                                                       kernel_initializer=initializer,
                                                       use_bias=False)

        self.batchnorm = tf.keras.layers.BatchNormalization()
        if self.apply_dropout:
            self.dropout = tf.keras.layers.Dropout(0.5)

        self.attention = SelfAttention3D(filters)

    def call(self, x1, x2, training):
        x = self.up_conv(x1)
        x = self.batchnorm(x, training=training)
        if self.apply_dropout:
            x = self.dropout(x, training=training)
        x = tf.nn.relu(x)
        x = self.attention(x)
        x = tf.concat([x, x2], axis=-1)
        return x


class DiscDownsample(tf.keras.Model):

    def __init__(self, filters, size, apply_batchnorm=True):
        super(DiscDownsample, self).__init__()
        self.apply_batchnorm = apply_batchnorm
        initializer = tf.random_normal_initializer(0., 0.02)

        self.conv1 = tf.keras.layers.Conv3D(filters,
                                            size,
                                            strides=2,
                                            padding='same',
                                            kernel_initializer=initializer,
                                            use_bias=False)
        if self.apply_batchnorm:
            self.batchnorm = tf.keras.layers.BatchNormalization()

    def call(self, x, training):
        x = self.conv1(x)
        if self.apply_batchnorm:
            x = self.batchnorm(x, training=training)
        x = tf.nn.leaky_relu(x)
        return x