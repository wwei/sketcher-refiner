
# coding: utf-8

# In[7]:

import tensorflow as tf
import sys
sys.path.append("../models_tf/")
from convolutions import Conv3D,Conv3DTranspose,Dense
from self_attention import SelfAttention3D
tf.enable_eager_execution()
tfe = tf.contrib.eager


# In[3]:

class Downsample(tf.keras.Model):
    
    def __init__(self, filters, size, apply_batchnorm=True):
        super(Downsample, self).__init__()
        self.apply_batchnorm = apply_batchnorm
        initializer = tf.random_normal_initializer(0., 0.02)

#         self.conv1 = tf.keras.layers.Conv2D(filters, 
#                                             (size, size), 
#                                             strides=2, 
#                                             padding='same',
#                                             kernel_initializer=initializer,
#                                             use_bias=False)
        self.conv1 = Conv3D(filters,
                            size, 
                            spectral_normalization=False, 
                            strides=2, 
                            padding='same', 
                            kernel_initializer=initializer, 
                            use_bias=False, 
                            activation=None)

        if self.apply_batchnorm:
            self.batchnorm = tf.keras.layers.BatchNormalization()
  
    def call(self, x, training):
        x = self.conv1(x)
        if self.apply_batchnorm:
            x = self.batchnorm(x, training=training)
        x = tf.nn.leaky_relu(x)
        return x 


# In[4]:

class Upsample(tf.keras.Model):
    
    def __init__(self, filters, size, apply_dropout=False):
        super(Upsample, self).__init__()
        self.apply_dropout = apply_dropout
        initializer = tf.random_normal_initializer(0., 0.02)

        self.up_conv = Conv3DTranspose(filters, 
                                       size, 
                                       spectral_normalization=False,
                                       strides=2, 
                                       padding='same', 
                                       kernel_initializer=initializer, 
                                       use_bias=False,
                                       activation=None)
        
        self.batchnorm = tf.keras.layers.BatchNormalization()
        if self.apply_dropout:
            self.dropout = tf.keras.layers.Dropout(0.5)

    def call(self, x1, x2, training):
        x = self.up_conv(x1)
        x = self.batchnorm(x, training=training)
        if self.apply_dropout:
            x = self.dropout(x, training=training)
        x = tf.nn.relu(x)
        x = tf.concat([x, x2], axis=-1)
        return x


# In[5]:

class Generator(tf.keras.Model):
    
    def __init__(self,output_channel):
        super(Generator, self).__init__()
        self.output_channel = output_channel
        initializer = tf.random_normal_initializer(0., 0.02)       

        self.down1 = Downsample(64, 4, apply_batchnorm=False)
        self.down2 = Downsample(128, 4)
        self.down3 = Downsample(256, 4)
        self.down4 = Downsample(512, 4)
        self.down5 = Downsample(512, 4)
#         self.down6 = Downsample(512, 4)
#         self.down7 = Downsample(512, 4)
#         self.down8 = Downsample(512, 4)

        self.up1 = Upsample(512, 4, apply_dropout=True)
        self.up2 = Upsample(256, 4)
        self.up3 = Upsample(128, 4)
        self.up4 = Upsample(64, 4)
#         self.up5 = Upsample(256, 4)
#         self.up6 = Upsample(128, 4)
#         self.up7 = Upsample(64, 4)

        self.last = Conv3DTranspose(self.output_channel, 4, strides=2, padding='same',kernel_initializer=initializer)
  
    @tf.contrib.eager.defun
    def call(self, x, training):
    # x shape == (bs, 128, 160, 128, nb_channel)    
        x1 = self.down1(x, training=training) # (bs, 64, 80, 64, 64)
        x2 = self.down2(x1, training=training) #(bs, 32, 40, 32, 128)
        x3 = self.down3(x2, training=training) # (bs,16, 20,16, 256)
        x4 = self.down4(x3, training=training) # (bs, 8, 10, 8, 512)
        x5 = self.down5(x4, training=training) # (bs, 4, 5, 4, 512)

        x6 = self.up1(x5, x4, training=training) # (bs, 8, 10, 8, 1024)
        x7 = self.up2(x6, x3, training=training) # (bs, 16, 20, 16, 512)
        x8 = self.up3(x7, x2, training=training) # (bs, 32, 40, 32, 256)
        x9 = self.up4(x8, x1, training=training) # (bs, 64, 80, 64, 128)
        

        x10 = self.last(x9) # (bs, 128, 160, 128, 1)
#         x16 = tf.nn.tanh(x16)

        return x10


# In[6]:

class DiscDownsample(tf.keras.Model):
    
    def __init__(self, filters, size, apply_batchnorm=True):
        super(DiscDownsample, self).__init__()
        self.apply_batchnorm = apply_batchnorm
        initializer = tf.random_normal_initializer(0., 0.02)
        
        self.conv1 = Conv3D(filters,
                    size, 
                    spectral_normalization=False, 
                    strides=2, 
                    padding='same', 
                    kernel_initializer=initializer, 
                    use_bias=False, 
                    activation=None)

    
        if self.apply_batchnorm:
            self.batchnorm = tf.keras.layers.BatchNormalization()
  
    def call(self, x, training):
        x = self.conv1(x)
        if self.apply_batchnorm:
            x = self.batchnorm(x, training=training)
        x = tf.nn.leaky_relu(x)
        return x 


# #did't finish for 3D medical image
# class Discriminator(tf.keras.Model):
#     
#     def __init__(self):
#         super(Discriminator, self).__init__()
#         initializer = tf.random_normal_initializer(0., 0.02)
# 
#         self.down1 = DiscDownsample(64, 4, False)
#         self.down2 = DiscDownsample(128, 4)
#         self.down3 = DiscDownsample(256, 4)
# 
#         # we are zero padding here with 1 because we need our shape to 
#         # go from (batch_size, 32, 32, 256) to (batch_size, 31, 31, 512)
#         self.zero_pad1 = tf.keras.layers.ZeroPadding2D()
#         self.conv = tf.keras.layers.Conv2D(512, 
#                                            (4, 4), 
#                                            strides=1, 
#                                            kernel_initializer=initializer, 
#                                            use_bias=False)
#         self.batchnorm1 = tf.keras.layers.BatchNormalization()
# 
#         # shape change from (batch_size, 31, 31, 512) to (batch_size, 30, 30, 1)
#         self.zero_pad2 = tf.keras.layers.ZeroPadding2D()
#         self.last = tf.keras.layers.Conv2D(1, 
#                                            (4, 4), 
#                                            strides=1,
#                                            kernel_initializer=initializer)
# 
#     @tf.contrib.eager.defun
#     def call(self, inp, tar, training):
#         # concatenating the input and the target
#         x = tf.concat([inp, tar], axis=-1) # (bs, 256, 256, channels*2)
#         x = self.down1(x, training=training) # (bs, 128, 128, 64)
#         x = self.down2(x, training=training) # (bs, 64, 64, 128)
#         x = self.down3(x, training=training) # (bs, 32, 32, 256)
# 
#         x = self.zero_pad1(x) # (bs, 34, 34, 256)
#         x = self.conv(x)      # (bs, 31, 31, 512)
#         x = self.batchnorm1(x, training=training)
#         x = tf.nn.leaky_relu(x)
# 
#         x = self.zero_pad2(x) # (bs, 33, 33, 512)
#         # don't add a sigmoid activation here since
#         # the loss function expects raw logits.
#         x = self.last(x)      # (bs, 30, 30, 1)
# 
#         return x
