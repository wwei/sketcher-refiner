from __future__ import division

import tensorflow as tf
from models_tf.models import FlexSelfAttention3D, ResDown

tf.enable_eager_execution()


class Discriminator(tf.keras.Model):

    def __init__(self):
        super(Discriminator, self).__init__()
        initializer = tf.random_normal_initializer(0., 0.02)

        self.conv1 = tf.keras.layers.Conv3D(16,
                                            4,
                                            strides=1,
                                            padding='same',
                                            kernel_initializer=initializer,
                                            use_bias=False) 
        self.resdown1 = ResDown(32,4)
        self.resdown2 = ResDown(64,4)
        self.fsa1 = FlexSelfAttention3D(64)
        self.resdown3 = ResDown(128,4)
        self.resdown4 = ResDown(256,4)

        self.last = tf.keras.layers.Conv3D(1,
                                           4,
                                           strides=1,
                                           padding='same',
                                           kernel_initializer=initializer)

    #    @tf.contrib.eager.defun
    def call(self, inp, tar, training):
        # concatenating the input and the target
        x = tf.concat([inp, tar], axis=-1)  # (bs, 64, 64, 64, 5)
        x1 = self.conv1(x)                          # (bs, 64, 64, 64, 16)
        x2 = self.resdown1(x1, training=training)   # (bs, 32, 32, 32, 32)
        x3 = self.resdown2(x2, training=training)   # (bs, 16, 16, 16, 64)
        x4 = self.fsa1(x3)                          # (bs, 16, 16, 16, 64)
        x5 = self.resdown3(x4, training=training)   # (bs, 8, 8, 8, 128)
        x6 = self.resdown4(x5, training=training)   # (bs, 4, 4, 4, 256)

        x7 = self.last(x6)                          # (bs, 4, 4, 4, 1)

        return x7
