from __future__ import division

import tensorflow as tf
from models_tf.models import FlexSelfAttention3D, ResDown, ResUp

tf.enable_eager_execution()


class Generator(tf.keras.Model):

    def __init__(self, output_channel):
        super(Generator, self).__init__()
        self.output_channel = output_channel
        initializer = tf.random_normal_initializer(0., 0.02)

        self.conv1 = tf.keras.layers.Conv3D(16,
                                            4,
                                            strides=1,
                                            padding='same',
                                            kernel_initializer=initializer,
                                            use_bias=False) 
        self.resdown1 = ResDown(32,4)
        self.resdown2 = ResDown(64,4)
        self.fsa1 = FlexSelfAttention3D(64)
        self.resdown3 = ResDown(128,4)
        self.resdown4 = ResDown(256,4)

        self.resup1 = ResUp(128,4 ,apply_dropout=True)
        self.resup2 = ResUp(64,4)
        self.fsa2 = FlexSelfAttention3D(64)
        self.resup3 = ResUp(32,4)
        self.resup4 = ResUp(16,4)

        self.conv2 = tf.keras.layers.Conv3D(self.output_channel,
                                            4,
                                            strides=1,
                                            padding='same',
                                            activation="relu",
                                            kernel_initializer=initializer,
                                            use_bias=False) 

    # @tf.contrib.eager.defun
    def call(self, x, training):
        # x shape == (bs, 64, 64, 64, nb_channel)
        x1 = self.conv1(x)                          # (bs, 64, 64, 64, 16)
        x2 = self.resdown1(x1, training=training)   # (bs, 32, 32, 32, 32)
        x3 = self.resdown2(x2, training=training)   # (bs, 16, 16, 16, 64)
        x4 = self.fsa1(x3)                          # (bs, 16, 16, 16, 64)
        x5 = self.resdown3(x4, training=training)   # (bs, 8, 8, 8, 128)
        x6 = self.resdown4(x5, training=training)   # (bs, 4, 4, 4, 256)

        x7 = self.resup1(x6, x5 ,training=training) # (bs, 8, 8, 8, 128)
        x8 = self.resup2(x7, x4 ,training=training) # (bs, 16, 16, 16, 64)
        x9 = self.fsa2(x8)                          # (bs, 16, 16, 16, 64)
        x10 = self.resup3(x9, x2 ,training=training)# (bs, 32, 32, 32, 32)
        x11 = self.resup4(x10, x1 ,training=training)# (bs, 64, 64, 64, 16)
        x12 = self.conv2(x11)

        return x12

