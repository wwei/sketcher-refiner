from __future__ import division

import os
import argparse
import numpy as np

from tensorflow.keras.utils import Progbar
import tensorflow as tf

from models_tf.d_model import Discriminator
from models_tf.g_model import Generator
from utils.utils import load_h5py_data, load_h5py_data_augmen, reconstruct_from_patches_3d, save_image
from utils.losses import discriminator_loss, generator_loss, calculate_weight


tf.enable_eager_execution()

parser = argparse.ArgumentParser()
parser.add_argument("--mode", dest='mode', type=str, help="specify which GAN")
parser.add_argument("--num_epochs_sketcher", dest='num_epochs_sketcher', type=int, default=400, help="specify number of epochs")
parser.add_argument("--num_epochs_refiner", dest='num_epochs_refiner', type=int, default=400, help="specify number of epochs")
parser.add_argument("--train_image_path", dest='train_image_path', type=str, help="specify path to training images")
parser.add_argument("--test_image_path", dest='test_image_path', type=str, help="specify path to test images")
parser.add_argument("--output_path", dest='output_path', type=str, help="specify path to save images")
parser.add_argument("--batch_size", dest='batch_size', type=float, default=1, help="specify batch size")
parser.add_argument("--checkpoint_dir_Sketcher", dest='checkpoint_dir_sketcher', type=str, default='./training_checkpoints_sketcher', help="specify the checkpoint for the Sketcher")
parser.add_argument("--checkpoint_dir_Refiner", dest='checkpoint_dir_refiner', type=str, default='./training_checkpoints_refiner', help="specify the checkpoint for the Refiner")


args = parser.parse_args()


def train_sketcher(args):
    num_epochs = args.num_epochs_sketcher
    path = args.train_image_path
    batch_size = args.batch_size
    saving_path = os.path.join(args.output_path,"sketcher")

    if not os.path.exists(saving_path):
        os.mkdir(saving_path)

    generator = Generator(output_channel=1)
    discriminator = Discriminator()
    generator_optimizer = tf.train.AdamOptimizer(2e-4, beta1=0.5)
    discriminator_optimizer = tf.train.AdamOptimizer(2e-4, beta1=0.5)


    # checkpoint_dir = args.checkpoint_dir_sketcher
    # checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
    # checkpoint = tf.train.Checkpoint(generator_optimizer=generator_optimizer,
    #                                  discriminator_optimizer=discriminator_optimizer,
    #                                  generator=generator,
    #                                  discriminator=discriminator)

    name_all, img_input_all, img_output_all, img_mask_all, affine_trans_all = load_h5py_data(
        os.path.join(path, Train_file))

    # data Augmentation
    img_input_all_augmen, img_output_all_augmen, img_mask_all_augmen = load_h5py_data_augmen(
        os.path.join(path, Train_file_augmen))

    num_people_train = len(name_all)

    # batch_size_data_augmen=4

    for epoch in range(num_epochs):
    	print('Epoch {} of {}'.format(epoch + 1, num_epochs))
        progress_bar = Progbar(target=num_people_train)

        gen_loss_all = []
        dis_loss_all = []

       	for p in range(num_people_train):
            progress_bar.update(p + 1)

            name = name_all[p]
            img_input = np.concatenate((img_input_all[p],img_input_all_augmen[p]))
            img_output = np.concatenate((img_output_all[p],img_output_all_augmen[p]))
            img_mask = np.concatenate((img_mask_all[p],img_mask_all_augmen[p]))   
            # img_input = img_input_all[p] 
            # img_output = img_output_all[p]
            # img_mask = img_mask_all[p]
            affine_trans = affine_trans_all[p]
 
    

            pred_train = -np.ones_like(img_output)

            for index in range(int(img_input.shape[0] / batch_size)):
                patch_mri = img_input[index * batch_size:(index + 1) * batch_size]
                patch_pet = img_output[index * batch_size:(index + 1) * batch_size]

                with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
                    patch_pet_fake = generator(patch_mri, training=True)

                    disc_real_output = discriminator(patch_mri, patch_pet, training=True)
                    disc_generated_output = discriminator(patch_mri, patch_pet_fake, training=True)

                    gen_loss = generator_loss(disc_generated_output, patch_pet_fake, patch_pet, weight=100, mask=1)
                    disc_loss = discriminator_loss(disc_real_output, disc_generated_output)


                generator_gradients = gen_tape.gradient(gen_loss,
                                                        generator.variables)
                discriminator_gradients = disc_tape.gradient(disc_loss,
                                                             discriminator.variables)

                generator_optimizer.apply_gradients(zip(generator_gradients,
                                                        generator.variables))
                discriminator_optimizer.apply_gradients(zip(discriminator_gradients,
                                                            discriminator.variables))
                gen_loss_all.append(gen_loss.numpy())
                dis_loss_all.append(disc_loss.numpy())


                if index < 36:
                    pred_train[index * batch_size:(index + 1) * batch_size] = patch_pet_fake


            # Reconstruct and save images 
         #    if epoch % 5 == 0:

         #    img_output_whole_fake = reconstruct_from_patches_3d(patches=pred_train, image_size=(128, 160, 128, 1),
         #                                                        extraction_step=32)
         #    img_output_whole_true = reconstruct_from_patches_3d(patches=img_output_all[p],
         #                                                        image_size=(128, 160, 128, 1), extraction_step=32)

        	# save_image(img_output_whole_fake, affine_trans, os.path.join(saving_path, name.decode('UTF-8') + "_" + str(epoch) + "_pred_.nii.gz"))
         #    save_image(img_output_whole_true, affine_trans, os.path.join(saving_path, name.decode('UTF-8') + "_" + str(epoch) + "_gt_.nii.gz"))

        print("+++++Training: Gen error= %f,  Dis Error= %f" % (
            np.mean(gen_loss_all), np.mean(dis_loss_all)))

	# checkpoint.save(file_prefix = checkpoint_prefix)

def train_refiner(args):
    num_epochs = args.num_epochs_refiner
    path = args.train_image_path
    batch_size = args.batch_size
    saving_path = os.path.join(args.output_path,"refiner")

    if not os.path.exists(saving_path):
        os.mkdir(saving_path)

    generator = Generator(output_channel=1)
    discriminator = Discriminator()
    generator_optimizer = tf.train.AdamOptimizer(2e-4, beta1=0.5)
    discriminator_optimizer = tf.train.AdamOptimizer(2e-4, beta1=0.5)


    # checkpoint_dir = args.checkpoint_dir_refiner
    # checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
    # checkpoint = tf.train.Checkpoint(generator_optimizer=generator_optimizer,
    #                                  discriminator_optimizer=discriminator_optimizer,
    #                                  generator=generator,
    #                                  discriminator=discriminator)

    name_all, img_input_all, img_output_all, img_mask_all, affine_trans_all = load_h5py_data(
        os.path.join(path, Train_file_sketcher))


    num_people_train = len(name_all)

    for epoch in range(num_epochs):
        print('Epoch {} of {}'.format(epoch + 1, num_epochs))
        progress_bar = Progbar(target=num_people_train)

        gen_loss_all = []
        dis_loss_all = []

        for p in range(num_people_train):
            progress_bar.update(p + 1)

            name = name_all[p]
            # img_input = np.concatenate((img_input_all[p],img_input_all_augmen[p]))
            # img_output = np.concatenate((img_output_all[p],img_output_all_augmen[p]))
            # img_mask = np.concatenate((img_mask_all[p],img_mask_all_augmen[p]))   
            img_input = img_input_all[p] 
            img_output = img_output_all[p]
            img_mask = img_mask_all[p]
            affine_trans = affine_trans_all[p]
 
            pred_train = -np.ones_like(img_output)

            for index in range(int(img_input.shape[0] / batch_size)):
                patch_mri = img_input[index * batch_size:(index + 1) * batch_size]
                patch_pet = img_output[index * batch_size:(index + 1) * batch_size]
                patch_les = img_mask[index * batch_size:(index + 1) * batch_size, :, :, :, 1:2]
                patch_wm = img_mask[index * batch_size:(index + 1) * batch_size, :, :, :, 0:1]
                # remove overlap
                patch_wm = patch_wm - (patch_wm * patch_les) 

                patch_mask_brain = np.zeros_like(patch_pet)
                patch_mask_brain[patch_pet>0]=1
                
                patch_other = patch_mask_brain-patch_wm-patch_les
                patch_other[patch_other<0]=0
                
                w= calculate_weight(patch_wm, patch_les, patch_other)

                with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
                    patch_pet_fake = generator(patch_mri, training=True)

                    disc_real_output = discriminator(patch_mri, patch_pet, training=True)
                    disc_generated_output = discriminator(patch_mri, patch_pet_fake, training=True)

                    gen_loss = generator_loss(disc_generated_output, patch_pet_fake, patch_pet, weight=100, mask=w)
                    disc_loss = discriminator_loss(disc_real_output, disc_generated_output)


                generator_gradients = gen_tape.gradient(gen_loss,
                                                        generator.variables)
                discriminator_gradients = disc_tape.gradient(disc_loss,
                                                             discriminator.variables)

                generator_optimizer.apply_gradients(zip(generator_gradients,
                                                        generator.variables))
                discriminator_optimizer.apply_gradients(zip(discriminator_gradients,
                                                            discriminator.variables))
                gen_loss_all.append(gen_loss.numpy())
                dis_loss_all.append(disc_loss.numpy())


                if index < 36:
                    pred_train[index * batch_size:(index + 1) * batch_size] = patch_pet_fake


            # Reconstruct and save images 
         #    if epoch % 5 == 0:

         #    img_output_whole_fake = reconstruct_from_patches_3d(patches=pred_train, image_size=(128, 160, 128, 1),
         #                                                        extraction_step=32)
         #    img_output_whole_true = reconstruct_from_patches_3d(patches=img_output_all[p],
         #                                                        image_size=(128, 160, 128, 1), extraction_step=32)

            # save_image(img_output_whole_fake, affine_trans, os.path.join(saving_path, name.decode('UTF-8') + "_" + str(epoch) + "_pred_.nii.gz"))
         #    save_image(img_output_whole_true, affine_trans, os.path.join(saving_path, name.decode('UTF-8') + "_" + str(epoch) + "_gt_.nii.gz"))

        print("+++++Training: Gen error= %f,  Dis Error= %f" % (
            np.mean(gen_loss_all), np.mean(dis_loss_all)))
        
    # checkpoint.save(file_prefix = checkpoint_prefix)


def main(args):
	if args.mode == 'Sketcher':
		train_sketcher(args)
	if args.mode == 'Refiner':
		train_refiner(args)
	else:
		raise 'Please specify which GAN need to be trained: Sketcher or Refiner.'

if __name__ == '__main__':
    main(args)





