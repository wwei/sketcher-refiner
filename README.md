# Sketcher-Refiner

This is the Tensorflow implementation for Sketcher-Refiner GANs. Each component is a conditional flexible self-attention GAN (CF-SAGAN) which is specifically adjusted for high-dimensional medical images. The proposed flexible self-attention layers are used for both discriminator and generator.

<p align="center"><img width="80%" src="img/cFSAGAN.png" /></p>

More technical details can be found in our following papers:

- [Predicting PET-derived Myelin Content from Multisequence MRI for Individual Longitudinal Analysis in Multiple Sclerosis](http://dx.doi.org/10.1016/j.neuroimage.2020.117308). NeuroImage, 2020. [[PDF](https://hal.inria.fr/hal-02922534/file/elsarticle-template.pdf)]

- [Predicting PET-derived Demyelination from Multimodal MRI using Sketcher-Refiner Adversarial Training for Multiple Sclerosis](http://dx.doi.org/10.1016/j.media.2019.101546). Medical Image Analysis, 2019. [[PDF](https://hal.archives-ouvertes.fr/hal-02276634/file/medima-hal_version.pdf)]

- [Learning Myelin Content in Multiple Sclerosis from Multimodal MRI through Adversarial Training](http://dx.doi.org/10.1007/978-3-030-00931-1_59). In MICCAI 2018. [[PDF](https://hal.inria.fr/hal-01810822/file/paper.pdf)]

Please cite our papers, if it is helpful for you:

- @article{WEI2020117308,
title = "Predicting PET-derived myelin content from multisequence MRI for individual longitudinal analysis in multiple sclerosis",
journal = "NeuroImage",
volume = "223",
pages = "117308",
year = "2020",
issn = "1053-8119",
author = "Wen Wei and Emilie Poirion and Benedetta Bodini and Matteo Tonietto and Stanley Durrleman and Olivier Colliot and Bruno Stankoff and Nicholas Ayache"
}

- @article{WEI2019101546,
title = "Predicting PET-derived demyelination from multimodal MRI using sketcher-refiner adversarial training for multiple sclerosis",
journal = "Medical Image Analysis",
volume = "58",
pages = "101546",
year = "2019",
issn = "1361-8415",
doi = "https://doi.org/10.1016/j.media.2019.101546",
author = "Wen Wei and Emilie Poirion and Benedetta Bodini and Stanley Durrleman and Nicholas Ayache and Bruno Stankoff and Olivier Colliot"
}

- @InProceedings{wen2018miccai,
author="Wei, Wen
and Poirion, Emilie
and Bodini, Benedetta
and Durrleman, Stanley
and Ayache, Nicholas
and Stankoff, Bruno
and Colliot, Olivier",
title="Learning Myelin Content in Multiple Sclerosis from Multimodal MRI Through Adversarial Training",
booktitle="MICCAI 2018",
year = "2018",
publisher="Springer",
address="Cham",
pages="514-522"
}

# How to run the code


1. Clone this repository:
    ```
    git clone git@gitlab.inria.fr:wwei/sketcher-refiner.git
    cd sketcher-refiner
    ```
2. Use `extract_patches_3d` to reshape a 3D image into a collection of patches. The resulting patches are allocated in a dedicated array. And then save the data as hdf5 format.
3. The basic blocks (_ResDown, ResUp_) stored in _models_tf/models.py_ are implemented as shown in the picture. Modify them to meet the specific need.
4. Adjust the architure of the generator in _models_tf/g_model.py_ if some changes are needed.
5. Adjust the architure of the discriminator in _models_tf/d_model.py_ if some changes are needed.
6. Set up the hyper-parameters in the main.py and run the code:
    ```
    python main.py
    ```
    by passing the following options:

    ```
      --mode MODE                                                       #specify which GAN   
      --num_epochs_sketcher NUM_EPOCHS_SKETCHER                         #specify number of epochs   
      --num_epochs_refiner NUM_EPOCHS_REFINER                           #specify number of epochs   
      --train_image_path TRAIN_IMAGE_PATH                               #specify path to training images   
      --test_image_path TEST_IMAGE_PATH                                 #specify path to test images   
      --output_path OUTPUT_PATH                                         #specify path to save images   
      --batch_size BATCH_SIZE                                           #specify batch size   
      --checkpoint_dir_Sketcher CHECKPOINT_DIR_SKETCHER                 #specify the checkpoint for the Sketcher   
      --checkpoint_dir_Refiner CHECKPOINT_DIR_REFINER                   #specify the checkpoint for the Refiner
    ```
PS: Some other modules have also been implemented, sucn as `U-Net, Self-Attention 2D/3D, DownSample/UpSample, etc`. _Wasserstein loss and gradient penalty loss_ have also been implemented. Feel free to use them. Good luck! 



